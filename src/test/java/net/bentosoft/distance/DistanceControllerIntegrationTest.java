package net.bentosoft.distance;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import net.bentosoft.graph.GraphTestBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class DistanceControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @InjectMocks
    private GraphTestBuilder builder;

    @Test
    public void shouldFindDistanceRouteABC() throws Exception {
        ResponseEntity<Distance> responseEntity = restTemplate.postForEntity("/distance",
                builder.newGraph("A", "B", "C"), Distance.class);
        Distance client = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(client);
        assertEquals(client.getDistance().intValue(), 9);
    }

    @Test
    public void shouldFindDistanceRouteAD() throws Exception {
        ResponseEntity<Distance> responseEntity = restTemplate.postForEntity("/distance", builder.newGraph("A", "D"),
                Distance.class);
        Distance client = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(client);
        assertEquals(client.getDistance().intValue(), 5);
    }

    @Test
    public void shouldFindDistanceRouteADC() throws Exception {
        ResponseEntity<Distance> responseEntity = restTemplate.postForEntity("/distance",
                builder.newGraph("A", "D", "C"), Distance.class);
        Distance client = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(client);
        assertEquals(client.getDistance().intValue(), 13);
    }

    @Test
    public void shouldFindDistanceRouteAEBCD() throws Exception {
        ResponseEntity<Distance> responseEntity = restTemplate.postForEntity("/distance",
                builder.newGraph("A", "E", "B", "C", "D"), Distance.class);
        Distance client = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(client);
        assertEquals(client.getDistance().intValue(), 22);
    }

    @Test
    public void shouldFindDistanceRouteAED() throws Exception {
        ResponseEntity<Distance> responseEntity = restTemplate.postForEntity("/distance",
                builder.newGraph("A", "E", "D"), Distance.class);
        Distance client = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(client);
        assertEquals(client.getDistance().intValue(), -1);
    }

    @Test
    public void shouldFindShortestDistanceBetweenAC() throws Exception {
        ResponseEntity<Distance> responseEntity = restTemplate.postForEntity("/distance/from/A/to/C",
                builder.newGraph(), Distance.class);
        Distance client = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(client);
        assertEquals(client.getDistance().intValue(), 9);
    }

    @Test
    public void shouldFindShortestDistanceBetweenBB() throws Exception {
        ResponseEntity<Distance> responseEntity = restTemplate.postForEntity("/distance/from/B/to/B",
                builder.newGraph(), Distance.class);
        Distance client = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(client);
        assertEquals(client.getDistance().intValue(), 0);
    }

}
