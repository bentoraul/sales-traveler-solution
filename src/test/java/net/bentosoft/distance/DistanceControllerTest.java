package net.bentosoft.distance;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.bentosoft.Application;
import net.bentosoft.graph.Graph;
import net.bentosoft.graph.GraphTestBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
public class DistanceControllerTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private GraphTestBuilder builder;

    private JacksonTester<Graph> jsonTester;

    @Before
    public void before() {
        JacksonTester.initFields(this, objectMapper);
    }

    @Test
    public void shouldFindDistanceRouteABC() throws Exception {
        String string = jsonTester.write(builder.newGraph("A", "B", "C")).getJson();
        mockMvc.perform(post("/distance").accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(string)).andExpect(status().isOk())
                .andExpect(jsonPath("$.distance", is(9)));
    }

    @Test
    public void shouldFindDistanceRouteAD() throws Exception {
        String string = jsonTester.write(builder.newGraph("A", "D")).getJson();
        mockMvc.perform(post("/distance").accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(string)).andExpect(status().isOk())
                .andExpect(jsonPath("$.distance", is(5)));
    }

    @Test
    public void shouldFindDistanceRouteADC() throws Exception {
        String string = jsonTester.write(builder.newGraph("A", "D", "C")).getJson();
        mockMvc.perform(post("/distance").accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(string)).andExpect(status().isOk())
                .andExpect(jsonPath("$.distance", is(13)));
    }

    @Test
    public void shouldFindDistanceRouteAEBCD() throws Exception {
        String string = jsonTester.write(builder.newGraph("A", "E", "B", "C", "D")).getJson();
        this.mockMvc
                .perform(post("/distance").accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE).content(string))
                .andExpect(status().isOk()).andExpect(jsonPath("$.distance", is(22)));
    }

    @Test
    public void shouldFindDistanceRouteAED() throws Exception {
        String string = jsonTester.write(builder.newGraph("A", "E", "D")).getJson();
        mockMvc.perform(post("/distance").accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(string)).andExpect(status().isOk())
                .andExpect(jsonPath("$.distance", is(-1)));
    }

    @Test
    public void shouldFindShortestDistanceBetweenAC() throws Exception {
        String string = jsonTester.write(builder.newGraph()).getJson();
        mockMvc.perform(post("/distance/from/A/to/C").accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(string)).andExpect(status().isOk())
                .andExpect(jsonPath("$.distance", is(9)));
    }

    @Test
    public void shouldFindShortestDistanceBetweenBB() throws Exception {
        String string = jsonTester.write(builder.newGraph()).getJson();
        mockMvc.perform(post("/distance/from/B/to/B").accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON_VALUE).content(string)).andExpect(status().isOk())
                .andExpect(jsonPath("$.distance", is(0)));
    }

}
