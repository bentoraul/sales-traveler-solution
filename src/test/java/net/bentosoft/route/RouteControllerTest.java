package net.bentosoft.route;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.bentosoft.graph.Graph;
import net.bentosoft.graph.GraphTestBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class RouteControllerTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private GraphTestBuilder builder;

    private JacksonTester<Graph> jsonTester;

    @Before
    public void before() {
        JacksonTester.initFields(this, objectMapper);
    }

    @Test
    public void shouldCallThePostMethodAndReturnRoutesBetweenAC() throws Exception {
        String string = jsonTester.write(builder.newGraph()).getJson();
        this.mockMvc
                .perform(post("/routes/from/A/to/C?maxStops=4").accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE).content(string))
                .andExpect(status().isOk()).andExpect(jsonPath("$.routes", hasSize(4)))
                .andExpect(jsonPath("$.routes[0].route", is("ABC"))).andExpect(jsonPath("$.routes[1].route", is("ADC")))
                .andExpect(jsonPath("$.routes[2].route", is("ADEBC")))
                .andExpect(jsonPath("$.routes[3].route", is("AEBC")));
    }

    @Test
    public void shouldCallThePostMethodAndReturnRoutesBetweenCC() throws Exception {
        String string = jsonTester.write(builder.newGraph()).getJson();
        this.mockMvc
                .perform(post("/routes/from/C/to/C?maxStops=3").accept(MediaType.APPLICATION_JSON)
                        .contentType(MediaType.APPLICATION_JSON_VALUE).content(string))
                .andExpect(status().isOk()).andExpect(jsonPath("$.routes", hasSize(2)))
                .andExpect(jsonPath("$.routes[0].route", is("CDC")))
                .andExpect(jsonPath("$.routes[1].route", is("CEBC")));
    }

}
