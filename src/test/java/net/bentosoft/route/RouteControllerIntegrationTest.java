package net.bentosoft.route;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import net.bentosoft.graph.GraphTestBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class RouteControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @InjectMocks
    private GraphTestBuilder builder;

    @Test
    public void shouldCallThePostMethodAndReturnRoutesBetweenAC() throws Exception {
        ResponseEntity<RoutePathRespose> responseEntity = restTemplate.postForEntity("/routes/from/A/to/C?maxStops=4",
                builder.newGraph(), RoutePathRespose.class);
        RoutePathRespose client = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(client);
        assertEquals(client.getRoutes().size(), 4);
        assertEquals(client.getRoutes().get(0).getRoute(), "ABC");
        assertEquals(client.getRoutes().get(1).getRoute(), "ADC");
        assertEquals(client.getRoutes().get(2).getRoute(), "ADEBC");
        assertEquals(client.getRoutes().get(3).getRoute(), "AEBC");

    }

    @Test
    public void shouldCallThePostMethodAndReturnRoutesBetweenCC() throws Exception {
        ResponseEntity<RoutePathRespose> responseEntity = restTemplate.postForEntity("/routes/from/C/to/C?maxStops=3",
                builder.newGraph(), RoutePathRespose.class);
        RoutePathRespose client = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(client);
        assertEquals(client.getRoutes().size(), 2);
        assertEquals(client.getRoutes().get(0).getRoute(), "CDC");
        assertEquals(client.getRoutes().get(1).getRoute(), "CEBC");

    }

}
