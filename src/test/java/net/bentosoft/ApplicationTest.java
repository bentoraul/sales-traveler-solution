package net.bentosoft;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import net.bentosoft.distance.DistanceController;
import net.bentosoft.graph.GraphConotroller;
import net.bentosoft.route.RouteController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTest {

    @Autowired
    private DistanceController distanceWebService;

    @Autowired
    private GraphConotroller graphWebService;

    @Autowired
    private RouteController routeWebService;

    @Test
    public void contextLoads() {
        assertThat(distanceWebService).isNotNull();
        assertThat(graphWebService).isNotNull();
        assertThat(routeWebService).isNotNull();
    }

}
