package net.bentosoft.graph;

public class GraphTestBuilder {

    public Graph newGraph(String... steps) {
        return new GraphBuilder().add("A", "B", 5).add("B", "C", 4).add("C", "D", 8).add("D", "C", 8).add("D", "E", 6)
                .add("A", "D", 5).add("C", "E", 2).add("E", "B", 3).add("A", "E", 7).setPath(steps).build();
    }

}
