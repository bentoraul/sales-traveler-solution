package net.bentosoft.graph;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GraphControllerIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @InjectMocks
    private GraphTestBuilder builder;

    @Test
    public void shouldCallTheGetMethodAndReturnOkStatus() throws Exception {
        shouldCallThePostMethodAndReturnCreatedStatus();
        ResponseEntity<Graph> responseEntity = restTemplate.getForEntity("/graph/1", Graph.class);
        Graph client = responseEntity.getBody();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(client);
        assertNotNull(client.getId());
        assertEquals(client.getId().intValue(), 1);
    }

    @Test
    public void shouldCallThePostMethodAndReturnCreatedStatus() throws Exception {
        ResponseEntity<Graph> responseEntity = restTemplate.postForEntity("/graph", builder.newGraph(), Graph.class);
        Graph client = responseEntity.getBody();
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertNotNull(client);
        assertNotNull(client.getId());
    }

}
