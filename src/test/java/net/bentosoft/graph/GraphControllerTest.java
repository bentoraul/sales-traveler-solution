package net.bentosoft.graph;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class GraphControllerTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private GraphTestBuilder builder;

    private JacksonTester<Graph> jsonTester;

    @Before
    public void before() {
        JacksonTester.initFields(this, objectMapper);
    }

    @Test
    public void shouldCallTheGetMethodAndReturnOkStatus() throws Exception {
        shouldCallThePostMethodAndReturnCreatedStatus();
        mockMvc.perform(
                get("/graph/1").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(jsonPath("$.data", notNullValue())).andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)));
    }

    @Test
    public void shouldCallThePostMethodAndReturnCreatedStatus() throws Exception {
        Graph graph = builder.newGraph();
        String string = jsonTester.write(graph).getJson();
        mockMvc.perform(post("/graph").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(string)).andExpect(jsonPath("$.data", hasSize(graph.getData().size())))
                .andExpect(jsonPath("$.id", notNullValue())).andExpect(status().isCreated());
    }

}
