package net.bentosoft.graph;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class GraphRepositoryTest {

    @Autowired
    private GraphRepository repository;

    @InjectMocks
    private GraphTestBuilder builder;

    @Test
    public void shouldSaveGraph() throws Exception {
        Graph graph = repository.save(builder.newGraph());
        assertThat(graph.getId()).isNotNull();
    }

    @Test
    public void shouldSaveGraphAndFindById() throws Exception {
        Graph graph = repository.save(builder.newGraph());
        assertThat(graph.getId()).isNotNull();
        Graph sameGraph = repository.findById(graph.getId());
        assertThat(graph).isEqualTo(sameGraph);
    }

}
