package net.bentosoft.distance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.springframework.stereotype.Service;

import net.bentosoft.graph.Graph;
import net.bentosoft.route.Route;

@Service
public class DistanceService {

    private Stack<String> path;

    private Set<String> track;

    private Graph graph;

    private String from;

    private String to;

    private Map<String, Integer> allPaths;

    private void build() {
        setAttributes();
        lookForPaths(graph, from, to, allPaths, 0);
    }

    private Distance findOutDistance() {
        int distance = 0;
        char[] path = null;
        for (String key : allPaths.keySet()) {
            int value = allPaths.get(key);
            if (distance > value || distance == 0) {
                distance = value;
                path = key.toCharArray();
            }
        }
        return distance == 0 ? new Distance(-1) : new Distance(distance, toStringArray(path));
    }

    public Distance getDistanceFor(List<String> path) {
        if (from.equals(to) || graph.getPath() == null || graph.getPath().size() < 2) {
            return new Distance(0);
        }
        build();
        Integer distance = allPaths.get(path.toString().replaceAll("\\W", ""));
        return new Distance(distance == null ? -1 : distance);
    }

    public Distance getShortestPath() {
        if (from.equals(to)) {
            return new Distance(0, Arrays.asList(new String[] { from }));
        }
        build();
        return findOutDistance();
    }

    private void lookForPaths(Graph graph, String from, String to, Map<String, Integer> allPaths, Integer distance) {
        path.push(from);
        track.add(from);
        if (from.equals(to)) {
            allPaths.put(path.toString().replaceAll("\\W", ""), distance);
        } else {
            nextStep(graph, from, to, allPaths, distance);
        }
        path.pop();
        track.remove(from);
    }

    private void nextStep(Graph graph, String from, String to, Map<String, Integer> allPaths, Integer distance) {
        for (Route route : graph.getData()) {
            if (!track.contains(route.getTarget()) && route.getSource().equals(from)) {
                lookForPaths(graph, route.getTarget(), to, allPaths, distance + route.getDistance());
            }
        }
    }

    private void setAttributes() {
        allPaths = new HashMap<>();
        path = new Stack<String>();
        track = new HashSet<String>();
    }

    public DistanceService setFrom(String from) {
        this.from = from;
        return DistanceService.this;
    }

    public DistanceService setGraph(Graph graph) {
        this.graph = graph;
        return DistanceService.this;
    }

    public DistanceService setTo(String to) {
        this.to = to;
        return DistanceService.this;
    }

    private List<String> toStringArray(char[] chars) {
        if (chars == null) {
            return null;
        }
        List<String> result = new ArrayList<>();
        for (char c : chars) {
            result.add(String.format("%s", c));
        }
        return result;
    }

}
