package net.bentosoft.distance;

import java.io.Serializable;
import java.util.List;

public class Distance implements Serializable {

    private static final long serialVersionUID = -5667199470203184622L;

    private Integer distance;

    private List<String> path;

    public Distance() {
        super();
    }

    public Distance(Integer distance) {
        super();
        this.distance = distance;
    }

    public Distance(Integer distance, List<String> path) {
        super();
        this.distance = distance;
        this.path = path;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Distance other = (Distance) obj;
        if (distance == null) {
            if (other.distance != null)
                return false;
        } else if (!distance.equals(other.distance))
            return false;
        if (path == null) {
            if (other.path != null)
                return false;
        } else if (!path.equals(other.path))
            return false;
        return true;
    }

    public Integer getDistance() {
        return distance;
    }

    public List<String> getPath() {
        return path;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((distance == null) ? 0 : distance.hashCode());
        result = prime * result + ((path == null) ? 0 : path.hashCode());
        return result;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public void setPath(List<String> path) {
        this.path = path;
    }

}
