package net.bentosoft.distance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.bentosoft.graph.Graph;
import net.bentosoft.graph.GraphException;
import net.bentosoft.graph.GraphService;

@RestController
@RequestMapping("distance")
public class DistanceController {

    @Autowired
    private DistanceService builder;

    @Autowired
    private GraphService service;

    @PostMapping(value = "/from/{from}/to/{to}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Distance> findDistanceBetweenTowns(@RequestBody Graph graph,
            @PathVariable("from") String from, @PathVariable("to") String to) {
        return new ResponseEntity<>(builder.setGraph(graph).setFrom(from).setTo(to).getShortestPath(), HttpStatus.OK);
    }

    @PostMapping(value = "/{id}/from/{from}/to/{to}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Distance> findDistanceBetweenTowns(@PathVariable("id") Integer id,
            @PathVariable("from") String from, @PathVariable("to") String to) {
        Graph graph = service.findById(id);
        if (graph != null) {
            return findDistanceBetweenTowns(graph, from, to);
        } else {
            throw new GraphException("NO SUCH GRAPH");
        }
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Distance> findDistanceForPath(@RequestBody Graph graph) {
        String from = graph.getPath().get(0);
        String to = graph.getPath().get(graph.getPath().size() - 1);
        return new ResponseEntity<>(builder.setGraph(graph).setFrom(from).setTo(to).getDistanceFor(graph.getPath()),
                HttpStatus.OK);
    }

    @PostMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Distance> findDistanceForPath(@RequestBody Graph graph, @PathVariable("id") Integer id) {
        Graph graphSaved = service.findById(id);
        if (graphSaved != null) {
            graphSaved.setPath(graph.getPath());
            return findDistanceForPath(graphSaved);
        } else {
            throw new GraphException("NO SUCH GRAPH");
        }
    }

}
