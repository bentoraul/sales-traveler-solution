package net.bentosoft.route;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

import net.bentosoft.graph.Graph;

@Entity
@Table(name = "tb_route")
@IdClass(RoutePrimaryKey.class)
public class Route implements Serializable {

    private static final long serialVersionUID = -7564257648800226969L;

    @Id
    private String source;

    @Id
    private String target;

    @Id
    @ManyToOne(targetEntity = Graph.class)
    @JoinColumn
    @JsonBackReference
    private Graph graph;

    @Id
    private Integer distance;

    public Route() {
        super();
    }

    public Route(String source, String target, Graph graph, Integer distance) {
        super();
        this.source = source;
        this.target = target;
        this.graph = graph;
        this.distance = distance;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Route other = (Route) obj;
        if (distance == null) {
            if (other.distance != null)
                return false;
        } else if (!distance.equals(other.distance))
            return false;
        if (graph == null) {
            if (other.graph != null)
                return false;
        } else if (!graph.equals(other.graph))
            return false;
        if (source == null) {
            if (other.source != null)
                return false;
        } else if (!source.equals(other.source))
            return false;
        if (target == null) {
            if (other.target != null)
                return false;
        } else if (!target.equals(other.target))
            return false;
        return true;
    }

    public Integer getDistance() {
        return distance;
    }

    public Graph getGraph() {
        return graph;
    }

    public String getSource() {
        return source;
    }

    public String getTarget() {
        return target;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((distance == null) ? 0 : distance.hashCode());
        result = prime * result + ((graph == null) ? 0 : graph.hashCode());
        result = prime * result + ((source == null) ? 0 : source.hashCode());
        result = prime * result + ((target == null) ? 0 : target.hashCode());
        return result;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public void setGraph(Graph graph) {
        this.graph = graph;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setTarget(String target) {
        this.target = target;
    }

}
