package net.bentosoft.route;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import org.springframework.stereotype.Service;

import net.bentosoft.graph.Graph;

@Service
public class RouteService {

    private Stack<String> path;

    private Set<String> track;

    private Graph graph;

    private String from;

    private String to;

    private Set<RoutePath> allPaths;

    private Integer maxStops;

    private void addPathToList(Set<RoutePath> allPaths) {
        String value = this.from.equals(this.to) ? String.format("%s%s", this.from, getPathString()) : getPathString();
        if ((maxStops == null || value.length() - 1 <= maxStops)) {
            allPaths.add(new RoutePath(value));
        }
    }

    public RouteService setFrom(String from) {
        this.from = from;
        return RouteService.this;
    }

    public List<RoutePath> getPathList() {
        setAttributes();
        if (from.equals(to)) {
            lookForPaths();
        } else {
            lookForPaths(graph, from, to, allPaths);
        }
        return new ArrayList<>(allPaths);
    }

    public RoutePathRespose getRoute() {
        List<RoutePath> list = getPathList();
        list.sort((routeA, routeB) -> routeA.getRoute().compareTo(routeB.getRoute()));
        return new RoutePathRespose(list);
    }

    public RouteService setGraph(Graph graph) {
        this.graph = graph;
        return RouteService.this;
    }

    private void lookForPaths() {
        for (Route route : graph.getData()) {
            if (route.getSource().equals(from)) {
                lookForPaths(graph, route.getTarget(), to, allPaths);
            }
        }
    }

    private void lookForPaths(Graph graph, String from, String to, Set<RoutePath> allPaths) {
        path.push(from);
        track.add(from);
        if (from.equals(to)) {
            addPathToList(allPaths);
        } else {
            nextStep(graph, from, to, allPaths);
        }
        path.pop();
        track.remove(from);
    }

    private void nextStep(Graph graph, String from, String to, Set<RoutePath> allPaths) {
        for (Route route : graph.getData()) {
            if ((!track.contains(route.getTarget()) && route.getSource().equals(from))) {
                lookForPaths(graph, route.getTarget(), to, allPaths);
            }
        }
    }

    private String getPathString() {
        return path.toString().replaceAll("\\W", "");
    }

    private void setAttributes() {
        allPaths = new HashSet<>();
        path = new Stack<String>();
        track = new HashSet<String>();
    }

    public RouteService setMaxStops(Integer maxStops) {
        this.maxStops = maxStops;
        return RouteService.this;
    }

    public RouteService setTo(String to) {
        this.to = to;
        return RouteService.this;
    }

}
