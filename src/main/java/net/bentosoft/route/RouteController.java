package net.bentosoft.route;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.bentosoft.graph.Graph;
import net.bentosoft.graph.GraphException;
import net.bentosoft.graph.GraphService;

@RestController
@RequestMapping("routes")
public class RouteController {

    @Autowired
    private RouteService builder;

    @Autowired
    private GraphService service;

    @PostMapping(value = "/from/{from}/to/{to}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findAvailableRoutesBetweenTowns(@RequestBody Graph graph,
            @PathVariable("from") String from, @PathVariable("to") String to,
            @RequestParam(value = "maxStops", required = false) Integer maxStops) {
        return new ResponseEntity<>(builder.setGraph(graph).setFrom(from).setTo(to).setMaxStops(maxStops).getRoute(),
                HttpStatus.OK);
    }

    @PostMapping(value = "/{id}/from/{from}/to/{to}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findAvailableRoutesBetweenTowns(@PathVariable("id") Integer id,
            @PathVariable("from") String from, @PathVariable("to") String to,
            @RequestParam(value = "maxStops", required = false) Integer maxStops) {
        Graph graph = service.findById(id);
        if (graph != null) {
            return findAvailableRoutesBetweenTowns(graph, from, to, maxStops);
        } else {
            throw new GraphException("NO SUCH GRAPH");
        }
    }

}
