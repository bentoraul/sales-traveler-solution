package net.bentosoft.route;

import java.io.Serializable;

import net.bentosoft.graph.Graph;

public class RoutePrimaryKey implements Serializable {

    private static final long serialVersionUID = 6959224212474696776L;

    private String source;

    private String target;

    private Graph graph;

    private Integer distance;

    public RoutePrimaryKey() {
        super();
    }

    public RoutePrimaryKey(String source, String target, Graph graph, Integer distance) {
        super();
        this.source = source;
        this.target = target;
        this.graph = graph;
        this.distance = distance;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RoutePrimaryKey other = (RoutePrimaryKey) obj;
        if (distance == null) {
            if (other.distance != null)
                return false;
        } else if (!distance.equals(other.distance))
            return false;
        if (graph == null) {
            if (other.graph != null)
                return false;
        } else if (!graph.equals(other.graph))
            return false;
        if (source == null) {
            if (other.source != null)
                return false;
        } else if (!source.equals(other.source))
            return false;
        if (target == null) {
            if (other.target != null)
                return false;
        } else if (!target.equals(other.target))
            return false;
        return true;
    }

    public Integer getDistance() {
        return distance;
    }

    public Graph getGraph() {
        return graph;
    }

    public String getSource() {
        return source;
    }

    public String getTarget() {
        return target;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((distance == null) ? 0 : distance.hashCode());
        result = prime * result + ((graph == null) ? 0 : graph.hashCode());
        result = prime * result + ((source == null) ? 0 : source.hashCode());
        result = prime * result + ((target == null) ? 0 : target.hashCode());
        return result;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public void setGraph(Graph graph) {
        this.graph = graph;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void setTarget(String target) {
        this.target = target;
    }

}
