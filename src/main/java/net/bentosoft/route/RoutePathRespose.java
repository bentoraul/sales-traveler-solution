package net.bentosoft.route;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RoutePathRespose implements Serializable {

    private static final long serialVersionUID = -7581776860059439326L;

    private List<RoutePath> routes;

    public RoutePathRespose() {
        super();
    }

    public RoutePathRespose(List<RoutePath> routes) {
        super();
        this.routes = routes;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RoutePathRespose other = (RoutePathRespose) obj;
        if (routes == null) {
            if (other.routes != null)
                return false;
        } else if (!routes.equals(other.routes))
            return false;
        return true;
    }

    public List<RoutePath> getRoutes() {
        if (routes == null) {
            routes = new ArrayList<>();
        }
        return routes;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((routes == null) ? 0 : routes.hashCode());
        return result;
    }

    public void setRoutes(List<RoutePath> routes) {
        this.routes = routes;
    }

}
