package net.bentosoft.route;

import java.io.Serializable;

public class RoutePath implements Serializable {

    private static final long serialVersionUID = 7174252238950033944L;

    private String route;

    public RoutePath() {
        super();
    }

    public RoutePath(String route) {
        super();
        this.route = route;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RoutePath other = (RoutePath) obj;
        if (route == null) {
            if (other.route != null)
                return false;
        } else if (!route.equals(other.route))
            return false;
        return true;
    }

    public String getRoute() {
        return route;
    }

    public Integer getStops() {
        return route.length() - 1;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((route == null) ? 0 : route.hashCode());
        return result;
    }

    public void setRoute(String route) {
        this.route = route;
    }

}
