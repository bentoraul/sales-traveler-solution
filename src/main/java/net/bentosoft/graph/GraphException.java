package net.bentosoft.graph;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No Such Graph")
public class GraphException extends RuntimeException {

    private static final long serialVersionUID = 3437064586734517138L;

    private String message;

    public GraphException() {
        super();
    }

    public GraphException(String message) {
        super(message);
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
