package net.bentosoft.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import net.bentosoft.route.Route;

public class GraphBuilder {

    private Graph graph = new Graph();

    private List<Route> routes = new ArrayList<>();

    public GraphBuilder add(String from, String to, Integer distance) {
        routes.add(new Route(from, to, graph, distance));
        return GraphBuilder.this;
    }

    public Graph build() {
        graph.setData(routes);
        return graph;
    }

    public GraphBuilder remove(Route route) {
        routes.remove(route);
        return GraphBuilder.this;
    }

    public GraphBuilder setPath(String... steps) {
        graph.setPath(Arrays.asList(steps));
        return GraphBuilder.this;

    }

}
