package net.bentosoft.graph;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GraphService {

    @Autowired
    private GraphRepository dao;

    public Graph findById(Integer id) {
        return dao.findById(id);
    }

    public Graph save(Graph graph) {
        graph.getData().stream().forEach(data -> {
            data.setGraph(graph);
        });
        return dao.save(graph);
    }

}
