package net.bentosoft.graph;

import org.springframework.data.repository.CrudRepository;

public interface GraphRepository extends CrudRepository<Graph, Integer> {

    Graph findById(Integer id);

}
