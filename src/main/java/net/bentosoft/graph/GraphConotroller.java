package net.bentosoft.graph;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("graph")
public class GraphConotroller {

    @Autowired
    private GraphService service;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Graph> retrieveGraphConfiguration(@PathVariable("id") Integer id) {
        Graph graph = service.findById(id);
        if (graph == null) {
            throw new GraphException("NO SUCH GRAPH");
        } else {
            return new ResponseEntity<>(graph, HttpStatus.OK);
        }
    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Graph> saveGraphConfiguration(@RequestBody Graph graph) {
        return new ResponseEntity<>(service.save(graph), HttpStatus.CREATED);
    }

}
